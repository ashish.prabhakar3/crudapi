const express = require('express');
const router = express.Router();
const brandController = require('../controller/brandController');
const joiSchemaValidation = require('../middleware/joiSchemaValidation');
const brandSchema = require('../apiSchema/brandSchema');

router.post('/', joiSchemaValidation.validateBody(brandSchema.createBrandSchema),
brandController.createBrand);

router.get('/:id', brandController.getBrandById);

router.put('/:id', joiSchemaValidation.validateBody(brandSchema.updateBrandSchema), brandController.updateBrand);

router.delete('/:id',brandController.deleteBrand);

router.get('/', joiSchemaValidation.validateQueryParams(brandSchema.getAllBrandsSchema), brandController.getAllBrands);

module.exports = router;
