const express = require('express');
const router = express.Router();
const orderController = require('../controller/orderController');
const joiSchemaValidation = require('../middleware/joiSchemaValidation');
const orderSchema = require('../apiSchema/orderSchema');

router.post('/', joiSchemaValidation.validateBody(orderSchema.createOrderSchema),
orderController.createOrder);

router.get('/:id', orderController.getOrderById);

router.get('/', joiSchemaValidation.validateQueryParams(orderSchema.getAllOrdersSchema), orderController.getAllOrders);

module.exports = router;
