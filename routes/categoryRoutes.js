const express = require('express');
const router = express.Router();
const categoryController = require('../controller/categoryController');
const joiSchemaValidation = require('../middleware/joiSchemaValidation');
const categorySchema = require('../apiSchema/categorySchema');

router.post('/', joiSchemaValidation.validateBody(categorySchema.createCategorySchema),
categoryController.createCategory);

router.get('/:id', categoryController.getCategoryById);

router.put('/:id', joiSchemaValidation.validateBody(categorySchema.updateCategorySchema), categoryController.updateCategory);

router.delete('/:id',categoryController.deleteCategory);

router.get('/', joiSchemaValidation.validateQueryParams(categorySchema.getAllCategoriesSchema), categoryController.getAllCategories);

module.exports = router;
