const express = require('express');
const router = express.Router();
const groupController = require('../controller/groupController');
const joiSchemaValidation = require('../middleware/joiSchemaValidation');
const groupSchema = require('../apiSchema/groupSchema');

router.post('/', joiSchemaValidation.validateBody(groupSchema.createGroupSchema),
groupController.createGroup);

router.get('/:id', groupController.getGroupById);

router.put('/:id', joiSchemaValidation.validateBody(groupSchema.updateGroupSchema), groupController.updateGroup);

router.delete('/:id',groupController.deleteGroup);

router.get('/', joiSchemaValidation.validateQueryParams(groupSchema.getAllGroupsSchema), groupController.getAllGroups);

module.exports = router;
