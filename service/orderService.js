const Order = require('../database/models/orderModel');

const { formatMongoData, checkObjectId } = require('../helper/dbHelper');
const constants = require('../constants');

module.exports.createOrder = async (serviceData) => {
    try {
        let order = new Order({...serviceData});
        let result = await order.save();
        return formatMongoData(result);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.getAllOrders = async ({skip = 0, limit = 10}) => {
    try {
        let orders = await Order.find({}).skip(parseInt(skip)).limit(parseInt(limit));
        return formatMongoData(orders);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.getOrderById = async ({id}) => {
    try {
        checkObjectId(id);
        let order = await Order.findById(id);
        if(!order){
            throw new Error(constants.orderMessage.ORDER_NOT_FOUND);
        }
        return formatMongoData(order);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.updateOrder = async ({id, updateInfo}) => {
    try {
        checkObjectId(id);
        let order = await Order.findOneAndUpdate(
            {_id: id},
            updateInfo,
            {new:true}
        )
        if(!order){
            throw new Error(constants.orderMessage.ORDER_NOT_FOUND);
        }
        return formatMongoData(order);
    } catch(error){
        throw new Error(error);
    }
}
