const Category = require('../database/models/categoryModel');

const { formatMongoData, checkObjectId } = require('../helper/dbHelper');
const constants = require('../constants');

module.exports.createCategory = async (serviceData) => {
    try {
        let category = new Category({...serviceData});
        let result = await category.save();
        console.log("service response", result);
        return formatMongoData(result);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.getAllCategories = async ({skip = 0, limit = 10}) => {
    try {
        let categories = await Category.find({}).skip(parseInt(skip)).limit(parseInt(limit));
        return formatMongoData(categories);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.getCategoryById = async ({id}) => {
    try {
        checkObjectId(id);
        let category = await Category.findById(id);
        if(!category){
            throw new Error(constants.categoryMessage.CATEGORY_NOT_FOUND);
        }
        return formatMongoData(category);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.updateCategory = async ({id, updateInfo}) => {
    try {
        checkObjectId(id);
        let category = await Category.findOneAndUpdate(
            {_id: id},
            updateInfo,
            {new:true}
        )
        if(!category){
            throw new Error(constants.categoryMessage.CATEGORY_NOT_FOUND);
        }
        return formatMongoData(category);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.deleteCategory = async ({id}) => {
    try {
        checkObjectId(id);
        let category = await Category.findByIdAndDelete(id);
        if(!category){
            throw new Error(constants.categoryMessage.CATEGORY_NOT_FOUND);
        }
        return formatMongoData(category);
    } catch(error){
        throw new Error(error);
    }
}
