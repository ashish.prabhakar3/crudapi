const Group = require('../database/models/groupModel');

const { formatMongoData, checkObjectId } = require('../helper/dbHelper');
const constants = require('../constants');

module.exports.createGroup = async (serviceData) => {
    try {
        let group = new Group({...serviceData});
        let result = await group.save();
        return formatMongoData(result);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.getAllGroups = async ({skip = 0, limit = 10}) => {
    try {
        let groups = await Group.find({}).skip(parseInt(skip)).limit(parseInt(limit));
        return formatMongoData(groups);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.getGroupById = async ({id}) => {
    try {
        checkObjectId(id);
        let group = await Group.findById(id);
        if(!group){
            throw new Error(constants.groupMessage.GROUP_NOT_FOUND);
        }
        return formatMongoData(group);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.updateGroup = async ({id, updateInfo}) => {
    try {
        checkObjectId(id);
        let group = await Group.findOneAndUpdate(
            {_id: id},
            updateInfo,
            {new:true}
        )
        if(!group){
            throw new Error(constants.groupMessage.GROUP_NOT_FOUND);
        }
        return formatMongoData(group);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.deleteGroup = async ({id}) => {
    try {
        checkObjectId(id);
        let group = await Group.findByIdAndDelete(id);
        if(!brand){
            throw new Error(constants.groupMessage.GROUP_NOT_FOUND);
        }
        return formatMongoData(group);
    } catch(error){
        throw new Error(error);
    }
}