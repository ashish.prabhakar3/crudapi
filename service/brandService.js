const Brand = require('../database/models/brandModel');

const { formatMongoData, checkObjectId } = require('../helper/dbHelper');
const constants = require('../constants');

module.exports.createBrand = async (serviceData) => {
    try {
        let brand = new Brand({...serviceData});
        let result = await brand.save();
        return formatMongoData(result);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.getAllBrands = async ({skip = 0, limit = 10}) => {
    try {
        let brands = await Brand.find({}).skip(parseInt(skip)).limit(parseInt(limit));
        return formatMongoData(brands);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.getBrandById = async ({id}) => {
    try {
        checkObjectId(id);
        let brand = await Brand.findById(id);
        if(!brand){
            throw new Error(constants.brandMessage.BRAND_NOT_FOUND);
        }
        return formatMongoData(brand);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.updateBrand = async ({id, updateInfo}) => {
    try {
        checkObjectId(id);
        let brand = await Brand.findOneAndUpdate(
            {_id: id},
            updateInfo,
            {new:true}
        )
        if(!brand){
            throw new Error(constants.brandMessage.BRAND_NOT_FOUND);
        }
        return formatMongoData(brand);
    } catch(error){
        throw new Error(error);
    }
}

module.exports.deleteBrand = async ({id}) => {
    try {
        checkObjectId(id);
        let brand = await Brand.findByIdAndDelete(id);
        if(!brand){
            throw new Error(constants.brandMessage.BRAND_NOT_FOUND);
        }
        return formatMongoData(brand);
    } catch(error){
        throw new Error(error);
    }
}