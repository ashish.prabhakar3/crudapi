const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    productName: String,
    catName: String,
    brandName: String,
    sizeName: String,
    groupName:String,
    description: String,
    price:Number,
    tax:Number
}, {
    timestamps: true,
    toObject: {
        transform: function(doc,ret,options){
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        }
    } 
});

module.exports = mongoose.model('product', productSchema);