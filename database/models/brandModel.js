const mongoose = require('mongoose');

const brandSchema = new mongoose.Schema({
    brandname: String
}, {
    timestamps: true,
    toObject: {
        transform: function(doc,ret,options){
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        }
    } 
});

module.exports = mongoose.model('brand', brandSchema);