const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    cName: String,
    cPhone: String,
    cEmail: String,
    cAddress: String,
    cCity: String,
    cPincode: String,    
    catName: String,
    productName: String,
    brandName: String,
    sizeName: String,
    groupName:String,
    description: String,
    price:Number,
    tax:Number
}, {
    timestamps: true,
    toObject: {
        transform: function(doc,ret,options){
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
            return ret;
        }
    } 
});

module.exports = mongoose.model('order', orderSchema);