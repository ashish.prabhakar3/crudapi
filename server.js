const express = require('express');
const dotEnv = require('dotenv');
const cors = require('cors');
const dbConnection = require('./database/connection');

dotEnv.config();

const app = express();

dbConnection();

app.use(cors());

const PORT = process.env.PORT || 3000;

const myMiddleware = (req,res,next) => {
    console.log("middleware executed");
    next();
}

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/api/v1/category', require('./routes/categoryRoutes'));
app.use('/api/v1/group', require('./routes/groupRoutes'));
app.use('/api/v1/brand', require('./routes/brandRoutes'));
app.use('/api/v1/product', require('./routes/productRoutes'));
app.use('/api/v1/order', require('./routes/orderRoutes'));

app.use(myMiddleware);

app.use('/', myMiddleware, (req,res) => {
    res.send("Express API working");
});

app.listen(PORT, () => {
    console.log(`server listening on port ${PORT} `);
});

//error handler middleware

app.use((err, req,res,next) => {
    console.log("error occured", err.stack);
    res.status(500).send({
        status:500,
        message: err.message,
        body: {}
    });
});
