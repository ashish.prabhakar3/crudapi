const Joi = require('@hapi/joi');

module.exports.createCategorySchema = Joi.object({
    catName: Joi.string().min(3).required(),
    parent: Joi.string().min(3).required() 
});

module.exports.getAllCategoriesSchema = Joi.object({
    skip: Joi.string(),
    limit: Joi.string()
});

module.exports.updateCategorySchema = Joi.object({
    catName: Joi.string().min(3),
    parent: Joi.string().min(3) 
});