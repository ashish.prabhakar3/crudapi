const Joi = require('@hapi/joi');

module.exports.createGroupSchema = Joi.object({
    groupName: Joi.string().min(3).required()
});

module.exports.getAllGroupsSchema = Joi.object({
    skip: Joi.string(),
    limit: Joi.string()
});

module.exports.updateGroupSchema = Joi.object({
    groupName: Joi.string().min(3)
});