const Joi = require('@hapi/joi');

module.exports.createOrderSchema = Joi.object({
    cName: Joi.string().min(3).required(),
    cPhone: Joi.string().min(3).required(),
    cEmail: Joi.string().min(3).required(),
    cAddress: Joi.string().min(3).required(),
    cCity: Joi.string().min(3).required(),
    cPincode: Joi.string().min(3).required(),
    catName: Joi.string().min(3).required(),
    productName: Joi.string().min(3).required(),
    brandName: Joi.string().min(3).required(),
    sizeName: Joi.string().min(3).required(),
    groupName: Joi.string(),
    description: Joi.string(),
    price: Joi.number(),
    tax: Joi.number()
});

module.exports.getAllOrdersSchema = Joi.object({
    skip: Joi.string(),
    limit: Joi.string()
});