const Joi = require('@hapi/joi');

module.exports.createProductSchema = Joi.object({
    productName: Joi.string().min(3).required(),
    catName: Joi.string().min(3).required(),
    brandName: Joi.string(),
    sizeName: Joi.string(),
    groupName: Joi.string(),
    description: Joi.string(),
    price: Joi.number(),
    tax: Joi.number() 
});

module.exports.getAllProductsSchema = Joi.object({
    catName:Joi.string(),
    groupName: Joi.string(),
    skip: Joi.string(),
    limit: Joi.string()
});

module.exports.updateProductSchema = Joi.object({
    productName: Joi.string().min(3),
    catName: Joi.string().min(3),
    brandName: Joi.string(),
    sizeName: Joi.string(),
    groupName: Joi.string(),
    description: Joi.string(),
    price: Joi.number(),
    tax: Joi.number() 
});