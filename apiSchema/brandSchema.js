const Joi = require('@hapi/joi');

module.exports.createBrandSchema = Joi.object({
    brandName: Joi.string().min(3).required(),
    catName: Joi.string().min(3).required() 
});

module.exports.getAllBrandsSchema = Joi.object({
    skip: Joi.string(),
    limit: Joi.string()
});

module.exports.updateBrandSchema = Joi.object({
    brandName: Joi.string().min(3),
    catName: Joi.string().min(3) 
});