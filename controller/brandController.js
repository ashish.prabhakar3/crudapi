const brandService = require('../service/brandService');
const constants = require('../constants');

module.exports.createBrand = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await brandService.createBrand(req.body);
        response.status = 200;
        response.message = constants.brandMessage.BRAND_CREATED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.getAllBrands = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await brandService.getAllBrands(req.query);
        response.status = 200;
        response.message = constants.brandMessage.BRAND_FETCHED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.getBrandById = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await brandService.getBrandById(req.params);
        response.status = 200;
        response.message = constants.brandMessage.BRAND_FETCHED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.updateBrand = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await brandService.updateBrand({
            id: req.params.id,
            updateInfo: req.body
        });
        response.status = 200;
        response.message = constants.brandMessage.BRAND_UPDATED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
    }
    return res.status(response.status).send(response);
}

module.exports.deleteBrand = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await brandService.deleteBrand(req.params);
        response.status = 200;
        response.message = constants.brandMessage.BRAND_DELETED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);
}