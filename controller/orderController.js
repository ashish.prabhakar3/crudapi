const orderService = require('../service/orderService');
const constants = require('../constants');

module.exports.createOrder = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await orderService.createOrder(req.body);
        response.status = 200;
        response.message = constants.orderMessage.ORDER_CREATED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
    }
    return res.status(response.status).send(response);

}

module.exports.getAllOrders = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await orderService.getAllOrders(req.query);
        response.status = 200;
        response.message = constants.orderMessage.ORDER_FETCHED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.getOrderById = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await orderService.getOrderById(req.params);
        response.status = 200;
        response.message = constants.productMessage.ORDER_FETCHED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
    }
    return res.status(response.status).send(response);

}

module.exports.cancelOrder = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await orderService.updateOrder({
            id: req.params.id,
            updateInfo: req.body
        });
        response.status = 200;
        response.message = constants.orderMessage.ORDER_CANCELLED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
    }
    return res.status(response.status).send(response);
}

