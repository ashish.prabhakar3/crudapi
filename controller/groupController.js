const groupService = require('../service/groupService');
const constants = require('../constants');

module.exports.createGroup = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await groupService.createGroup(req.body);
        response.status = 200;
        response.message = constants.groupMessage.GROUP_CREATED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.getAllGroups = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await groupService.getAllGroups(req.query);
        response.status = 200;
        response.message = constants.groupMessage.GROUP_FETCHED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.getGroupById = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await groupService.getGroupById(req.params);
        response.status = 200;
        response.message = constants.groupMessage.GROUP_FETCHED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.updateGroup = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await groupService.updateGroup({
            id: req.params.id,
            updateInfo: req.body
        });
        response.status = 200;
        response.message = constants.groupMessage.GROUP_UPDATED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
    }
    return res.status(response.status).send(response);
}

module.exports.deleteGroup = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await groupService.deleteGroup(req.params);
        response.status = 200;
        response.message = constants.groupMessage.GROUP_DELETED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);
}