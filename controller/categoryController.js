const categoryService = require('../service/categoryService');
const constants = require('../constants');

module.exports.createCategory = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await categoryService.createCategory(req.body);
        response.status = 200;
        //response.message = 'Category created successfully';
        response.message = constants.categoryMessage.CATEGORY_CREATED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.getAllCategories = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await categoryService.getAllCategories(req.query);
        response.status = 200;
        //response.message = 'Category created successfully';
        response.message = constants.categoryMessage.CATEGORY_FETCHED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.getCategoryById = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await categoryService.getCategoryById(req.params);
        response.status = 200;
        //response.message = 'Category created successfully';
        response.message = constants.categoryMessage.CATEGORY_FETCHED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}

module.exports.updateCategory = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await categoryService.updateCategory({
            id: req.params.id,
            updateInfo: req.body
        });
        response.status = 200;
        //response.message = 'Category created successfully';
        response.message = constants.categoryMessage.CATEGORY_UPDATED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);
}

module.exports.deleteCategory = async (req,res) => {
    let response = {...constants.defaultServerResponse};
    try {
        const responseFromService = await categoryService.deleteCategory(req.params);
        response.status = 200;
        response.message = constants.categoryMessage.CATEGORY_DELETED;
        response.body = responseFromService;
    } catch(error) {
        console.log("error at controller end");
        //response.status = 400;
        response.message = error.message;
        //response.body = {};
    }
    return res.status(response.status).send(response);

}