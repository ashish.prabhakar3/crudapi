module.exports = {
    defaultServerResponse: {
        status : 400,
        message: '',
        body: {}
    },
    categoryMessage: {
        CATEGORY_CREATED: 'Category created successfully',
        CATEGORY_FETCHED: 'Categories retrieved successfully',
        CATEGORY_NOT_FOUND: 'Category Not Found',
        CATEGORY_UPDATED: 'Category updated successfully',
        CATEGORY_DELETED: 'Category deleted successfully',
    },
    brandMessage: {
        BRAND_CREATED: 'Brand created successfully',
        BRAND_FETCHED: 'Brand retrieved successfully',
        BRAND_NOT_FOUND: 'Brand Not Found',
        BRAND_UPDATED: 'Brand updated successfully',
        BRAND_DELETED: 'Brand deleted successfully',
    },
    groupMessage: {
        GROUP_CREATED: 'Group created successfully',
        GROUP_FETCHED: 'Group retrieved successfully',
        GROUP_NOT_FOUND: 'Group Not Found',
        GROUP_UPDATED: 'Group updated successfully',
        GROUP_DELETED: 'Group deleted successfully',
    },
    productMessage: {
        PRODUCT_CREATED: 'Product created successfully',
        PRODUCT_FETCHED: 'Product retrieved successfully',
        PRODUCT_NOT_FOUND: 'Product Not Found',
        PRODUCT_UPDATED: 'Product updated successfully',
        PRODUCT_DELETED: 'Product deleted successfully',
    },
    orderMessage: {
        ORDER_CREATED: 'Order created successfully',
        ORDER_FETCHED: 'Order retrieved successfully',
        ORDER_NOT_FOUND: 'Order Not Found',
        ORDER_CANCELLED: 'Order updated successfully',
        ORDER_DELETED: 'Order deleted successfully',
    },
    requestValidationMessage: {
        BAD_REQUEST: 'Invalid fields'
    },
    databaseMessage: {
        INVALID_ID: 'Invalid Id'
    }
}